// 17/05/2024
// Dijkstra Algorithm
// Finished

const ORDER: usize = 5;

fn get_non_closed_adjs(mat: &[Vec<u8>], closed_list: &[bool; 5], node_idx: usize) 
-> Vec<usize> {
    let mut adjs: Vec<usize> = Vec::new();
    for (adj, row) in mat[node_idx].iter().enumerate() {
        if *row != 0 && !closed_list[adj] {
            adjs.push(adj);
        }
    }
    return adjs;
}

fn get_lowest_estimated_non_closed_adj(estimated: [u8; 5], closed_list: [bool; 5]) 
-> usize {
    let mut lowest_estimated = u8::MAX;
    // Never will return 0;
    let mut node: usize = 0;
    for (node_idx, est) in estimated.iter().enumerate() {
        if !closed_list[node_idx] && *est < lowest_estimated {
            lowest_estimated = *est;
            node = node_idx;
        }
    }
    return node;
}

struct Result {
    estimated: [u8; ORDER],
    precedent: [usize; ORDER],
}

fn dijkstra(mat: &[Vec<u8>], mut node_idx: usize) -> Result {
    let mut closed_list = [false; ORDER];
    let mut estimated = [u8::MAX; ORDER];
    let mut precedent = [0; ORDER];
    estimated[node_idx] = 0;
    precedent[node_idx] = 0;
    while closed_list.iter().any(|val| *val == false) {
        let adjs = get_non_closed_adjs(&mat, &closed_list, node_idx);
        for adj in adjs {
            if mat[node_idx][adj]+estimated[node_idx] <= estimated[adj] {
                estimated[adj] = mat[node_idx][adj]+estimated[node_idx];
                precedent[adj] = node_idx;
            }
        }
        closed_list[node_idx] = true;
        node_idx = get_lowest_estimated_non_closed_adj(estimated, closed_list);
    }

    Result { estimated, precedent, }
}

fn display(result: &Result) {
    println!();
    print!("     ");
    for label in 1..=ORDER {
        print!("{:2} ", label);
    }
    println!();
    print!("est: ");
    for e in result.estimated {
        print!("{:2} ", e);
    }
    println!();
    print!("pre: ");
    for e in result.precedent {
        print!("{:2} ", e);
    }
    println!();
}

fn main() {
    let mut mat = vec![
        vec![0, 5, 0, 0, 1],
        vec![0, 0, 1, 0, 0],
        vec![0, 0, 0, 2, 3],
        vec![0, 0, 0, 0, 0],
        vec![0, 1, 0, 5, 0],
    ];
    let mut expected_est: [u8; ORDER] = [0, 2, 3, 5, 1];
    let mut expected_pre: [usize; ORDER]  = [0, 4, 1, 2, 0];
    let mut result = dijkstra(&mat, 0);
    display(&result);
    for i in 0..ORDER {
        assert_eq!(result.estimated[i], expected_est[i]);
        assert_eq!(result.precedent[i], expected_pre[i]);
    }

    mat = vec![
        vec![0, 10, 0, 5, 0],
        vec![0, 0, 1, 2, 0],
        vec![0, 0, 0, 0, 6],
        vec![0, 3, 9, 0, 2],
        vec![7, 0, 4, 0, 0],
    ];
    expected_est = [0, 8, 9, 5, 7];
    expected_pre = [0, 3, 1, 0, 3];
    result = dijkstra(&mat, 0);
    display(&result);
    for i in 0..ORDER {
        assert_eq!(result.estimated[i], expected_est[i]);
        assert_eq!(result.precedent[i], expected_pre[i]);
    }
}